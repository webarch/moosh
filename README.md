# Ansible Moosh Role

An Ansible role to install [Moosh](https://moosh-online.com/).

## Copyright

Copyright 2020-2025 Chris Croome, &lt;[chris@webarchitects.co.uk](mailto:chris@webarchitects.co.uk)&gt;.

This role is released under [the same terms as Ansible itself](https://github.com/ansible/ansible/blob/devel/COPYING), the [GNU GPLv3](LICENSE).
